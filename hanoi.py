def empty(stapel):
    return len(stapel) == 0

def top(stapel):
    if len(stapel) > 0:
        return stapel[len(stapel) - 1]
    else:
        return 0

def printStapelListe(liste):
    steine = ["   |   ","   #   ","  ###  "," ##### "]
    zeile = ""
    for y in range(0,4):
        for x in range(0,3):

            if (3-y) < len(liste[x]):
                zeile += steine[liste[x][3-y]]
            else:
                zeile += steine[0]
        print(zeile)
        zeile = ""

    print("---------------------")
    print("   1      2      3")
    print("")

stapelListe = [[3,2,1],[],[]]
printStapelListe(stapelListe)
zaehler = 0

while len(stapelListe[2]) != 3:
    eingabe = input("Nächster Zug? ").split()
    von = int(eingabe[0]) - 1
    nach = int(eingabe[1]) - 1

    if empty(stapelListe[nach]) or top(stapelListe[von]) < top(stapelListe[nach]):
        stein = stapelListe[von].pop()
        stapelListe[nach].append(stein)
        zaehler = zaehler + 1
    else:
        print("Ungültiger Zug!")
        continue

    printStapelListe(stapelListe)
print("Spiel gelöst in ", zaehler, " Zügen")