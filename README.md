# Die Türme von Hanoi

Bei dem Spiel "Die Türme von Hanoi" gibt es drei Stapel und auf dem ersten Stapel liegen n Steine. Wir beginnen mit 3 Steinen. Das Ziel des Spiels ist es, alle Steine auf den letzten Stapel zu bringen, wobei folgende Regeln zu berücksichtigen sind:

* bei jedem Zug darf ein Stein auf einen anderen Stapel gelegt werden, wenn
  * der Zielstapel leer ist
  * oder der der oberste Stein des Zielstapels größer ist als der neue Stein

Das Ergebnis soll am Ende in folgender Form dargestellt werden

```
    |       |       |
    #       |       |
   ###      |       |
  #####     |       |
-------------------------
    1       2       3
```

Das Spiel soll anhand der folgenden Aufgaben schrittweise entwickelt werden.

## Aufgabe 1

Erstelle ein einfaches Modell des Spielfeldes und gib es auf der Konsole aus.

## Lösung 1

[Link auf den ersten Commit](https://gitlab.com/python-beispiele/python-hanoi/-/commit/7381e5027dfd4f74678252a328c239231f1d4d24)

## Aufgabe 2

Solange nicht alle Steine auf dem letzten Stapel liegen
* Frag nach dem nächsten Zug in Form von `von` Stapelnummer `nach` Stapelnummer, z.B. `1 2`
* ermittle daraus die Nummer des Ursprungsstapels und des Zielstapels
* entferne das oberste Element des Ursprungsstapels und lege es auf den Zielstapel
* gib das neue Spielfeld auf dem Bildschirm aus

## Lösung 2

[Link auf den zweiten Commit](https://gitlab.com/python-beispiele/python-hanoi/-/commit/2a6eb46e395a84e2241450b2d4634c6dacf68e41)

## Aufgabe 3

Prüfe bei der Eingabe die oben genannten Regelen, um Fehleingaben zu vermeiden.

## Lösung Aufgabe 3

[Link auf dritten Commit](https://gitlab.com/python-beispiele/python-hanoi/-/commit/1d43a92f751098327818288fce9619ca6b9b5c53)

## Aufgabe 4

Zähle die Anzahl der gültigen Züge und gib sie am Ende des Spiels aus.

## Lösung 4

[Link auf vierten Commit](https://gitlab.com/python-beispiele/python-hanoi/-/commit/18a79ce019258b547c45921fa88177c33a93f55b)

## Aufgabe 5

Schreibe eine Funktion, welche das Spielfeld grafisch im Terminal ausgibt.

## Lösung 5

[Link auf fünften Commit](https://gitlab.com/python-beispiele/python-hanoi/-/commit/8f2ff6e61d32d3864d9c36a4785c60f898fe5715)